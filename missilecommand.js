//TODO bullet speed is godawful.... how to speed up and yet retain the coords system
//TODO need to add more spawns as time goes on
//TODO need to have them split on random and more often as time goes on

var shipImage = new Image();
shipImage.src = "cannon.jpg";

var canvas2 = document.getElementById("canvas2");
missile = [];
bullets = [];
explosions = [];
player = {
    health: 5,
    score: 0,
    bullets: 25
}

//create first spawn
create2(Math.random() * canvas2.width, 0, false, Math.random() * canvas2.width, canvas2.height);

function loop2() {
    if (player.health < 1) {
        alert("U GOT ASPLODED!!!!!!");
        return;
    }
    window.requestAnimationFrame(loop2);
    update2();
    kill2();
    draw2();
}

function update2() {
    //find where the missile is meant to go and then each step works out how it needs to get there
    for (var i in missile) {
        var miss = missile[i];
        var travelpX = 0;
        if (miss.destinationX < miss.x) {
            travelpX = (miss.destinationX - miss.x) / (miss.destinationY - miss.y);
            miss.x += travelpX;
        } else if (miss.destinationX > miss.x) {
            travelpX = (miss.x - miss.destinationX) / (miss.destinationY - miss.y);
            miss.x -= travelpX;
        }
        miss.y++;
    }
    //this is the inverse of above, find where the bomb is meant to go and then each step works out how it needs to get there
    for (var i in bullets) {
        var bul = bullets[i];
        var travelx = 0;
        if (bul.destinationX < bul.x) {
            travelx = (bul.destinationX - bul.x) / (bul.destinationY - bul.y);
            bul.x -= travelx;
        } else if (bul.destinationX > bul.x) {
            travelx = (bul.x - bul.destinationX) / (bul.destinationY - bul.y);
            bul.x += travelx;
        }
        if (bul.destinationX == bul.x && bul.destinationY == bul.y) {
            bullets.splice(i, 1);
            document.getElementById("boom").load();
            document.getElementById("boom").play();
            explosions.push({
                x: bul.x,
                y: bul.y,
                radius: 15,
                color: "red",
                lifetime: 0
            });
        }
        bul.y--;
    }

    //make explosion
    for (var e in explosions) {
        var expl = explosions[e];
        expl.radius++
        if (expl.radius > 25) {
            explosions.splice(e, 1);
        }
    }
}

function kill2() {
//deal with the death of particles
    for (var i in missile) {
        var miss = missile[i];
        if (miss.y > canvas2.height || miss.x < 0 || miss.x > canvas2.width) {
            if (miss.x > 0 && miss.x < canvas2.width) {
                player.health--;
            }
            missile.splice(i, 1);
            create2(Math.random() * canvas2.width, 0, false, Math.random() * canvas2.width, canvas2.height);
        }

        for (var e in explosions) {
            var expl = explosions[e];
            if (miss.x > expl.x - expl.radius &&
                miss.x < expl.x + expl.radius &&
                miss.y > expl.y - expl.radius &&
                miss.y < expl.y + expl.radius) {

                player.score++;
                missile.splice(i, 1);
                create2(Math.random() * canvas2.width, 0, false, Math.random() * canvas2.width, canvas2.height);
            }
        }
    }
}

function draw2() {
    var ctx = canvas2.getContext("2d");
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas2.width, canvas2.height);
    for (var i in missile) {
        var miss = missile[i];
        ctx.beginPath();
        ctx.arc(miss.x, miss.y, miss.radius, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fillStyle = miss.color;
        ctx.fill();
    }
    for (var j in bullets) {
        var bul = bullets[j];
        ctx.beginPath();
        ctx.arc(bul.x, bul.y, bul.radius, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fillStyle = bul.color;
        ctx.fill();
    }
    ctx.fillText("Score: " + player.score, 400, 30);
    ctx.fillText("Health: " + player.health, 400, 50);
    ctx.fillText("Bullets: " + player.bullets, 400, 70);
    ctx.drawImage(shipImage, canvas2.width / 2 - 15, canvas2.height - 50, 50, 50);
    for (var e in explosions) {
        var expl = explosions[e];
        ctx.beginPath();
        ctx.arc(expl.x, expl.y, expl.radius, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fillStyle = expl.color;
        ctx.fill();
    }
}

function create2(xcoord, ycoord, child, destX, destY) {
    missile.push({
        x: xcoord,
        y: ycoord,
        speed: Math.random() * 2,
        radius: 5 + Math.random() * 5,
        color: "white",
        child: child,
        destinationX: destX,
        destinationY: destY
    });
}

function fire(dx, dy) {
    document.getElementById("laser").load();
    document.getElementById("laser").play();


    //make sure we can only have one bullet alive at a time.
    if (bullets.length == 0) {
        player.bullets--;
        bullets.push({
            x: canvas2.width / 2,
            y: canvas2.height,
            speed: 3,
            radius: 5 + Math.random() * 5,
            destinationX: dx,
            destinationY: dy,
            color: "white",
            child: true
        });
    }
}

//set fire command to fire at our location.
canvas2.addEventListener('click', function (evt) {
        var canvasX = evt.pageX;
        var canvasY = evt.pageY;
        fire(canvasX, canvasY);
    }
    , false);

loop2();