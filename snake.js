(function(){
  //CONSTANTS
var BOARD_WIDTH = 40;
var BOARD_HEIGHT = 30;
var INCREASE_SPEED_AFTER_MS = 13000;
var INITIAL_TAIL_LENGTH = 5;
var INITIAL_SPEED = 5;

var DIRECTION = {
    NORTH: 0,
    EAST: 1,
    SOUTH: 2,
    WEST: 3
};

// snake for game (re)start
function Snake() {
    this.x = Math.floor(BOARD_WIDTH / 2);
    this.y = Math.floor(BOARD_HEIGHT / 2);
    this.dir = DIRECTION.EAST;
    this.speed = INITIAL_SPEED;
    this.tail = [];
    this.tailLength = INITIAL_TAIL_LENGTH;
}

// food for the snake to eat
// HRMMM1 what happens if this matches an exising food?
function Food() {
    this.x = Math.floor(Math.random() * BOARD_WIDTH);
    this.y = Math.floor(Math.random() * BOARD_HEIGHT);
}

var gameLoopTimer;
var timePlayedMs;
var snake = new Snake();
var food = [];

function gameLoop() {
    move();
    eat();
    render();
    recordTime();
    dieOrLive();
}

function move() {

    // shift/grow the tail
    snake.tail.push({
        x: snake.x,
        y: snake.y,
        dir: snake.dir
    });
    if (snake.tail.length > snake.tailLength) {
        snake.tail.shift();
    }

    // move the head
    switch (snake.dir) {
        case DIRECTION.NORTH:
            snake.y--;
            break;
        case DIRECTION.EAST:
            snake.x++;
            break;
        case DIRECTION.SOUTH:
            snake.y++;
            break;
        case DIRECTION.WEST:
            snake.x--;
            break;
    }
}

function eat() {

    // eat the food, grow the tail, generate new food
    for (var i = 0; i < food.length; i++) {
        if (food[i].x == snake.x && food[i].y == snake.y) {
            snake.tailLength++;
            // HRMMM2 why does this new food sometimes disappear, especially when inspecting/debugging?
            food[i] = new Food();
            break;
        }
    }
}

function render() {

    var board = $("#board");
    board.focus();
    board.empty();
    for (var y = 0; y < BOARD_HEIGHT; y++) {
        var row = $('<div class="row">');
        for (var x = 0; x < BOARD_WIDTH; x++) {
            var content = "";
            if (snake.x == x && snake.y == y) {
                content = "<img src ='head" + snake.dir + ".png'/>"
            } else {
                for (var t = 0; t < snake.tail.length; t++) {
                    if (snake.tail[t].x == x && snake.tail[t].y == y) {
                        content = "<img src ='body" + snake.tail[t].dir + ".png'/>";
                        break;
                    }
                }
            }

            for (var f = 0; f < food.length; f++) {
                if (food[f].x == x && food[f].y == y) {
                    content = "<img src ='food.png'/>"
                }
            }

            row.append("<div class='bodycell'> " + content + "</div>");
        }
        board.append(row);
    }
}

function recordTime() {

    timePlayedMs += 1000 / snake.speed;

    if (timePlayedMs > INCREASE_SPEED_AFTER_MS) {
        clearInterval(gameLoopTimer);
        snake.speed++;
        gameLoopTimer = window.setInterval(gameLoop, 1000 / snake.speed);
    }

    // HRMMM3 how can we increase the number of foods over time?
//    if (timePlayedMs > 30000 / snake.speed && food != null) {
//        generateFood();
//    }
}

function dieOrLive() {

    var dead = false;

    // wall collision death
    if (snake.x > (BOARD_WIDTH - 1) || snake.x < 0 || snake.y > (BOARD_HEIGHT - 1) || snake.y < 0) {
        dead = true;
    }

    // tail collision death
    for (var i = 0; i < snake.tail.length; i++) {
        if (snake.tail[i].x == snake.x && snake.tail[i].y == snake.y) {
            dead = true;
        }
    }

    if (dead) {
        clearInterval(gameLoopTimer);

        var score = Math.max(0, snake.tail.length - INITIAL_TAIL_LENGTH);

        $(".modal-body").append("Your score is " + score);
        $('#scoreModal').modal('show')
    }
}

$(".start").on("click", function () {

    $(".modal-body").empty();
    clearInterval(gameLoopTimer);

    timePlayedMs = 0;
    snake = new Snake();
    food = [];
    food.push(new Food());
    food.push(new Food());

    render();

    gameLoopTimer = window.setInterval(gameLoop, 1000 / snake.speed);
});

$(document).keydown(function () {

    // stop left, up, right, down from moving screen around
    if (event.which == 37 || event.which == 38 || event.which == 39 || event.which == 40) {
        event.preventDefault();
    }

    // rotate left
    if (event.which == 37 || event.which == 65) {
        if (snake.dir != 0) {
            snake.dir--;
        } else {
            snake.dir = 3;
        }
    }

    // rotate right
    else if (event.which == 39 || event.which == 68) {
        if (snake.dir != 3) {
            snake.dir++
        } else {
            snake.dir = 0;
        }
    }
});

// initial display
render();
})()